#!/usr/bin/env bash

function machine() {
case "$(uname -s)" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:$(uname -s)"
esac
echo "$machine"
}

yesno() {
    if [ "$(machine)" == "Linux" ] ; then
		if [ -z "$(which kdialog)" ]; then
		    echo "Error: kdialog not found"
		fi
        PROCEED=$(kdialog --yesno " $* новых слов,\n создать колоду?  " 20 60 && echo Yes)
    elif [ $(machine) == "Cygwin" ] ; then
        # https://daviddeley.com/autohotkey/parameters/parameters.htm
        PROCEED=$(cscript /Nologo dialogs/yesno.vbs $*)
    else
        echo "error: yesno $(machine)"
        exit
    fi
}

select-srp() {
    if [ $(machine) == "Linux" ] ; then
        SRP="$(kdialog --radiolist "select spaced repetition program:" 1 "anki" on  2  mnemosyne off)"
    elif [ $(machine) == "Cygwin" ] ; then
        # https://daviddeley.com/autohotkey/parameters/parameters.htm
        # SRP=$(cscript /Nologo radiolist.vbs)
        echo "error: select-srp not implemented on $(machine)"
    else
        echo "error: select-srp() $(machine)"
    fi
}

function getgdhistory() {
if [ $(machine) == "Linux" ] ; then
    GD_H=$HOME/.local/share/goldendict/history
    if [ ! -f $GD_H ]; then
        GD_H=$HOME/.goldendict/history
    fi
#     echo $GD_H
fi

if [ $(machine) == "Cygwin" ] ; then
    # https://cygwin.com/cygwin-ug-net/cygpath.html
    # XPATH="$(cygpath -C ANSI -w "${1}")";
    GD_H="$HOMEDRIVE$HOMEPATH\Application Data\GoldenDict\history"
fi

if [ ! -f "$GD_H" ]; then
    echo -e "\nerror: missing GD_HISTORY_FILE\n"
    exit
fi
echo $GD_H
}

function getgdconfig() {
if [ $(machine) == "Linux" ] ; then
    if [[ -d $HOME/.config/goldendict ]]; then
        GD_C=$HOME/.config/goldendict/config
    else
        # first run, no config yet...
        if [[ ! $GD_FIRSTRUN ]]; then
            GD_FIRSTRUN=true
            goldendict &
            sleep 5
            killall goldendict
            getgdconfig
        fi
    fi
fi
if [ $(machine) == "Cygwin" ] ; then
    # https://cygwin.com/cygwin-ug-net/cygpath.html
    # XPATH="$(cygpath -C ANSI -w "${1}")";
    # TODO: test it...
    GD_C="$HOMEDRIVE$HOMEPATH\Application Data\GoldenDict\config"
fi

echo $GD_C
}

function gettimevars() {
    TIME_MS=$(echo $(($(date +%s%N)/1000000)) )
    EPOCH=$(echo $TIME_MS|  cut -c 1-10)
    DECK_TIMESTAMP=$(date -d @$EPOCH +'%Y%m%d%H%M%S')
    DATE_TIME==$(date -d @$EPOCH +'%Y-%m-%d_%H:%M:%S')
    TODAY=$(date -d @$EPOCH +'%Y-%m-%d')
}

function time_ms(){
    echo $(($(date +%s%N)/1000000))
}

function configjson(){
if [[ $XDG_CONFIG_HOME ]]; then
    if [ ! -d "$XDG_CONFIG_HOME/dfh" ]; then
        mkdir -p $XDG_CONFIG_HOME/dfh
    fi
    CONFIG=$XDG_CONFIG_HOME/dfh/config.json
else
    if [ ! -d ~/.config/dfh ]; then
        mkdir -p ~/.config/dfh
    fi
    CONFIG=~/.config/dfh/config.json
fi
if [[ ! -f $CONFIG ]]; then
cat << EOF  > $CONFIG
{"showSRPDialog": false, "SRP": 1, "showDeckNameDialog": true, "deckName": "", "translator":"google", "tts":"google", "YANDEX-API-KEY":"12345", "LINGVO-API-KEY":"12345", "langFrom": "it", "langTo": "ru"}
EOF
fi

echo $CONFIG
}

function outdir() {
if [[ $XDG_CACHE_HOME ]]; then
    OUTDIR=$XDG_CACHE_HOME/dfh
else
    OUTDIR=~/.cache/dfh
fi
echo $OUTDIR
}

