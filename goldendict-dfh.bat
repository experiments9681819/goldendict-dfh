@echo off

if not DEFINED IS_MINIMIZED set IS_MINIMIZED=1 && start "" /min "%~dpnx0" %* && exit

cd %~dp0
SET PATH=%~dp0;c:\cygwin\bin;%PATH%
bash  goldendict-dfh.sh

exit

