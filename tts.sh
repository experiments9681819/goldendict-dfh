#!/bin/bash

source $DFHHOME/dfhlib.sh

OUTDIR="$(outdir)/sounds"
SOUNDFILE="$OUTDIR/"$*".mp3"

if [ "$#" -lt 1 ];then
        echo "Usage: $0 word to translate"
        exit 1
fi

if  [ ! -d "$OUTDIR" ] || [ ! -f "$SOUNDFILE" ]; then
    #bash trans.sh $*  >/dev/null 2>&1 
#     echo "do trans..."
    bash $DFHHOME/trans.sh $*  # WTF?
fi

test -s "$SOUNDFILE"
if [ $? -eq 1 ]; then
#     echo "removing zero lenght file:    rm "$SOUNDFILE""
    rm "$SOUNDFILE"
fi

if [ $(machine) == "Linux" ] ; then
    if [[ $(which mpv) ]]; then
        mpv "$SOUNDFILE"  >/dev/null 2>&1
    elif [[ $(which mplayer) ]]; then
        mplayer -ao alsa -really-quiet -noconsolecontrols "$SOUNDFILE"
    else
        echo "error: no sound player found, install mplayer | mpv"
    fi
elif [ $(machine) == "Cygwin" ] ; then
	#cygstart --minimize "$SOUNDFILE"
	mplayer -really-quiet -noconsolecontrols "$(cygpath -w "$SOUNDFILE")" 
else
    echo -e "\nerror: tts.sh error \n"
    exit
fi

