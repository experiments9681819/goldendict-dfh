#!/usr/bin/env bash

echo "historySync.sh is hack to synchronize trans-shell to goldendict history..."

# shellcheck source=/home/vic/Projects/goldendict-dfh-experiment/src/dfh/dfhlib.sh
source "$DFHHOME"/dfhlib.sh

OUTDIR="$(outdir)"
DB_FILE="$OUTDIR/dfh.sqlite"

echo "DB_FILE $DB_FILE"

# HISTORY_EDITED="$1" #history file


TRANS_HISTORY=$( sqlite3 "$DB_FILE" "SELECT word FROM history;" )

echo -e "translate-shell history: \n-------\n$TRANS_HISTORY\n-------"
echo -e "goldendict history file: \n-------\n$(getgdhistory)\n-------"
echo -e "goldendict history content: \n-------\n$(cat $(getgdhistory))\n-------"
# exit
IFS_BAK=$IFS
IFS=$'\n'
for entry in $TRANS_HISTORY
do
    KEEP=$(grep -w "$entry" $(getgdhistory) | awk -F'[0-9]+\\W' '{print $2}')
    if [ ! -z "$KEEP" ] ; then
        echo "keep: $KEEP" 
    else
        echo "del: $entry"
        sqlite3 "$DB_FILE" <<EOF
            DELETE FROM history WHERE word = '$entry';
EOF
        rm -f "$OUTDIR/sounds/$entry.mp3"
        echo "deleted: $entry"
    fi
done

IFS=$IFS_BAK

echo "sync script exited..."
