#!/usr/bin/env bash

#DFHHOME=/opt/dfh
DFHHOME=~/Projects/goldendict-dfh-experiment/src/dfh
export DFHHOME

# shellcheck source=/home/vic/Projects/goldendict-dfh-experiment/src/dfh/dfhlib.sh
source $DFHHOME/dfhlib.sh

DB_FILE=$(outdir)/dfh.sqlite

# GD_HISTORY_FILE=$(getgdhistory)

GD=$(which goldendict)
if [ -z "$GD" ] ;then
	echo -e "\n goldendict not found\n"
	exit
fi

GD_UNWRAPPED=$(ps ax|grep -w goldendict$ | grep -v 'grep\|bash' | awk '{print $1}')
if [[ $GD_UNWRAPPED ]];then echo "goldendict is running, exit first (Ctrl-Q)"; exit;fi

### prep config
if [[ -f "$(getgdconfig)~" ]]; then
sed  '
s/^ <\/programs>$/  <program id="0b59b5abe574d7725e722dc37f7dc036" icon="" name="trans" type="2" enabled="1" commandLine="trans.sh %GDWORD%"\/>\n  <program id="667a629443169478afc8676fca6910f9" icon="" name="tts" type="0" enabled="1" commandLine="tts.sh %GDWORD%"\/>\n&/

s/^ <dictionaryOrder id="0" name="">$/&\n  <dictionary name="trans">0b59b5abe574d7725e722dc37f7dc036<\/dictionary>\n  <dictionary name="tts">667a629443169478afc8676fca6910f9<\/dictionary>/

' $(getgdconfig)~ > $(getgdconfig)
else
sed -i~ '
s/^ <\/programs>$/  <program id="0b59b5abe574d7725e722dc37f7dc036" icon="" name="trans" type="2" enabled="1" commandLine="trans.sh %GDWORD%"\/>\n  <program id="667a629443169478afc8676fca6910f9" icon="" name="tts" type="0" enabled="1" commandLine="tts.sh %GDWORD%"\/>\n&/

s/^ <dictionaryOrder id="0" name="">$/&\n  <dictionary name="trans">0b59b5abe574d7725e722dc37f7dc036<\/dictionary>\n  <dictionary name="tts">667a629443169478afc8676fca6910f9<\/dictionary>/

' $(getgdconfig)
fi

PATH="$DFHHOME:$PATH" "$GD" --log-to-file > goldendict.log 2>&1
# %HOMEDRIVE%%HOMEPATH%\Application Data\GoldenDict\gd_log.txt
#                                                   history
#                                                   config

echo -e "\n\n\ngoldendict exited...\n"

if [ ! -d $(outdir) ]; then
    echo "info: no DATA, translate something..."
    exit
fi

bash $DFHHOME/historySync.sh

# list of words
UNPROCESSED=$( sqlite3  "$DB_FILE" << EOF
    SELECT id, word, translation FROM  history;
    SELECT id, word, translation FROM words WHERE processed IS NULL or processed = 0;
EOF
)
if [ -z "$UNPROCESSED" ]; then
	echo "info: no new words in history, exiting"
    exit
fi
UNPROCESSED_COUNT=$(echo -e "$UNPROCESSED"| wc -l)
echo -e "UNPROCESSED:\n$UNPROCESSED\n"

yesno $UNPROCESSED_COUNT
if [ -z $PROCEED ]; then 
    exit
fi


if [ "$(jq '.showSRPDialog' $(configjson))" == "true" ] ; then
    select-srp
else
    SRP=$(jq '.SRP' $(configjson))
fi

if [ "$SRP" == "1" ] ; then 
    bash $DFHHOME/anki.sh
elif [ "$SRP" == "2" ] ; then 
    bash $DFHHOME/mnemosyne.sh
else
    echo -e "\nerror: bad SRP | user cancels\n"
    exit
fi

### restore config
# cp "$(getgdconfig)~" $(getgdconfig)
echo "mv "$(getgdconfig)~" $(getgdconfig)"

echo "goldendict-dfh: Done."
exit

