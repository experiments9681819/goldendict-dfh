#!/usr/bin/env bash

# https://serverfault.com/questions/573946/how-can-i-send-a-message-to-the-systemd-journal-from-the-command-line
# echo 'hello' | systemd-cat -t goldendict-dfh -p info
# echo 'hello' | systemd-cat -t goldendict-dfh -p warning
# echo 'hello' | systemd-cat -t goldendict-dfh -p emerg
#
# journalctl -f -o verbose

# https://serverfault.com/questions/923435/how-to-filter-journalctl-output-by-process-name
#  You can use any systemd journal field as filter by specifying <FIELD_NAME>=<VALUE>.
# The following fields are useful for this question:
# _COMM=, _EXE=, _CMDLINE=
# The name, the executable path, and the command line of the process the journal entry originates from.
# So in order to filter on the command name, use journalctl -f _COMM=<command-name>

# echo 'hello' | systemd-cat -t goldendict-dfh -p info
# journalctl -f _COMM=echo

# journalctl --output=json --since -10m >> journalctl.json
# https://www.freedesktop.org/software/systemd/python-systemd/journal.html
