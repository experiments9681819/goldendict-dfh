set WshShell = WScript.CreateObject("WScript.Shell")
scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
vsDebug scriptdir
WshShell.CurrentDirectory=scriptdir

call setup


Sub setup()
	set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")

	DPath= WshShell.SpecialFolders("Desktop")
	shortcut = DPath & "\goldendict-dfh.lnk"

	If (fso.FileExists(shortcut)) Then
	   vsDebug  shortcut & " exists."
	Else
	   vsDebug path & " doesn't exist. Creating..."
	   call CreateDesktop
	End If
End Sub

Sub CreateDesktop()
	dim DPath, LinkDesktop
	DPath= WshShell.SpecialFolders("Desktop")
	set LinkDesktop = WshShell.CreateShortcut(DPath & "\goldendict-dfh.lnk")
	LinkDesktop.Arguments = " --log-to-file"	'--group-name=
	LinkDesktop.Description = "GoldenDict deck from history"
'FIXME copyfile: scriptdir & "\goldendict.ico" to system32 
	LinkDesktop.IconLocation =  scriptdir & "\goldendict.ico"
	LinkDesktop.TargetPath = scriptdir & "\goldendict-dfh.bat"
	LinkDesktop.WindowStyle = 2
	LinkDesktop.WorkingDirectory = scriptdir
	LinkDesktop.Save()
End Sub


Function vsDebug( msg )
	strScriptHost = LCase(Wscript.FullName)
	If Not Right(strScriptHost, 11) = "wscript.exe" Then
		Wscript.Echo "Debug: " & Now() & " -> " & msg
	End If
End Function
