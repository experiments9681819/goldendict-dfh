#!/bin/bash

if ! test -f testAnki.sh; then
  echo -e "run from the folder testAnki.sh is in: \nbash testAnki.sh"
  exit 1
fi

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac


if [ ${machine} == "Linux" ] ; then 
    GD_HISTORY_FILE=$HOME/.local/share/goldendict/history # FIXME
elif [ ${machine} == "Cygwin" ] ; then
	GD_HISTORY_FILE="$HOMEDRIVE$HOMEPATH\Application Data\GoldenDict\history"
else
    echo -e "\nerror: GD_HISTORY_FILE\n"
    exit
fi

echo "---$GD_HISTORY_FILE---"



bash  clearProcessed.sh
bash anki.sh




 
