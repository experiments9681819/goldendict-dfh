#!/usr/bin/env bash
# shellcheck source=/home/vic/Projects/goldendict-dfh-experiment/src/dfh/dfhlib.sh
source "$DFHHOME"/dfhlib.sh

# FIXME $(jq .langFrom settings.json | tr -d '"')         ???
LANG_FROM=$(echo -n $(jq .langFrom $(configjson)) | tr -d '"')
LANG_TO=$(echo -n $(jq .langTo $(configjson)) | tr -d '"')


echo -e anki.sh: Started, run on $(machine)


gettimevars
# echo ---$EPOCH---

# set deck name from settings
DECK_NAME=$(echo $(jq .deckName $(configjson)) | tr -d '"')
if [ "$DECK_NAME" == "" ] || [ "$DECK_NAME" == "null" ]; then
  DECK_NAME=$(echo $(jq .langFrom $(configjson)) | tr -d '"')
fi
DECK_NAME_UI=""


OUTDIR=$(outdir)
DB_FILE=$OUTDIR/dfh.sqlite

echo -e "\nDB_FILE=$OUTDIR/dfh.sqlite\n"

TEMPDIR=$(mktemp -d)
ANKI_DB=$TEMPDIR/collection.anki2
CARDS_NOTES_STMT=""

PROCEED=""
DECK_OUTDIR=""

# dirty hack for cygwin...
DECK_OUTDIR_FILE=""
cat $DFHHOME/ankidb.sql
sqlite3 $ANKI_DB  "$(cat $DFHHOME/ankidb.sql)"

getexistingdirectory() {
    if [ $(machine) == "Linux" ] ; then
        DECK_OUTDIR=$(kdialog --getexistingdirectory "$HOME/Desktop")
    elif [ $(machine) == "Cygwin" ] ; then
        DECK_OUTDIR="$(cscript /Nologo dialogs/getexistingdirectory.vbs)"
		DECK_OUTDIR_FILE=$DECK_OUTDIR
    else
        echo "error: getexistingdirectory $(machine)"
    fi
}

getdeckname() {
    if [ $(machine) == "Linux" ] ; then
        DECK_NAME_UI=$(kdialog --title "Input dialog" --inputbox "deck name" "$DECK_NAME")
    elif [ $(machine) == "Cygwin" ] ; then
        DECK_NAME_UI="$(cscript /Nologo dialogs/getdeckname.vbs $DECK_NAME)"
    else
        echo "error: getdeckname $(machine)"
    fi
}

function guid(){
    GUID=""
    chars='qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCNM1234567890'
    for i in {1..10} ; do
        GUID=$GUID"${chars:RANDOM%${#chars}:1}"
    done
}

# add items from history to apkg
# https://github.com/ankidroid/Anki-Android/wiki/Database-Structure#notes
PROCESSED=""
IFS_BAK=$IFS
IFS=$'\n'

# prep media json
MEDIA_JSN='{'

# list of words
UNPROCESSED=$( sqlite3  "$DB_FILE" << EOF
    SELECT id, word, translation FROM  history;
    SELECT id, word, translation FROM words WHERE processed IS NULL or processed = 0;
EOF
)

# prep ankidb col table
id=1
crt=1647831600
mod=1651317910769
scm=1651317910708
ver=11
dty=0
usn=0
ls=0

conf=$(jq -n --compact-output '{"curModel":1651313675153,"schedVer":2,"timeLim":0,"nextPos":1,"curDeck":1,"newSpread":0,"creationOffset":-120,"activeDecks":[1],"addToCur":true,"dayLearnFirst":false,"dueCounts":true,"sortBackwards":false,"sortType":"noteFld","estTimes":true,"collapseTime":1200}')

# https://habr.com/ru/post/445166/
# .card {
#   font-family: arial;
#   font-size: 20px;
#   text-align: center;
#   color: black;
#   background-color: white;
# 
#   -webkit-hyphens: auto;
#   -webkit-hyphenate-limit-before: 3;
#   -webkit-hyphenate-limit-after: 3;
#   -webkit-hyphenate-limit-chars: 6 3 3;
#   -webkit-hyphenate-limit-lines: 2;
#   -webkit-hyphenate-limit-last: always;   
#   -webkit-hyphenate-limit-zone: 8%;
# 
#   -moz-hyphens: auto;
#   -moz-hyphenate-limit-chars: 6 3 3;
#   -moz-hyphenate-limit-lines: 2;  
#   -moz-hyphenate-limit-last: always;
#   -moz-hyphenate-limit-zone: 8%;
# 
#   -ms-hyphens: auto;
#   -ms-hyphenate-limit-chars: 6 3 3;
#   -ms-hyphenate-limit-lines: 2;
#   -ms-hyphenate-limit-last: always;   
#   -ms-hyphenate-limit-zone: 8%;
# 
#   hyphens: auto;
#   hyphenate-limit-chars: 6 3 3;
#   hyphenate-limit-lines: 2;
#   hyphenate-limit-last: always;   
#   hyphenate-limit-zone: 8%;
# 
#   overflow: visible;
#   overflow-wrap: break-word;
# }

dfh_model_id="1651313675153"
dfh_model_name="Basic-DFH"
#dfh_model_css=".card {\n font-family: arial;\n font-size: 40px;\n text-align: center;\n color: black;\n background-color: white;\n hyphens: auto;\n -ms-hyphens: auto;\n -moz-hyphens: auto;\n -webkit-hyphens: auto;\n overflow-x: visible;\n}\n"
dfh_model_css=".card {\n  font-family: arial;\n  font-size: 40px;\n  text-align: center;\n  color: black;\n  background-color: white;\n\n  -webkit-hyphens: auto;\n  -webkit-hyphenate-limit-before: 3;\n  -webkit-hyphenate-limit-after: 3;\n  -webkit-hyphenate-limit-chars: 6 3 3;\n  -webkit-hyphenate-limit-lines: 2;\n  -webkit-hyphenate-limit-last: always;   \n  -webkit-hyphenate-limit-zone: 8%;\n\n  -moz-hyphens: auto;\n  -moz-hyphenate-limit-chars: 6 3 3;\n  -moz-hyphenate-limit-lines: 2;  \n  -moz-hyphenate-limit-last: always;\n  -moz-hyphenate-limit-zone: 8%;\n\n  -ms-hyphens: auto;\n  -ms-hyphenate-limit-chars: 6 3 3;\n  -ms-hyphenate-limit-lines: 2;\n  -ms-hyphenate-limit-last: always;   \n  -ms-hyphenate-limit-zone: 8%;\n\n  hyphens: auto;\n  hyphenate-limit-chars: 6 3 3;\n  hyphenate-limit-lines: 2;\n  hyphenate-limit-last: always;   \n  hyphenate-limit-zone: 8%;\n\n  overflow: visible;\n  overflow-wrap: break-word;\n}"

models=$(jq -n --compact-output '{"1651314490797":{"id":1651314490797,"name":"Basic","type":0,"mod":0,"usn":0,"sortf":0,"did":null,"tmpls":[{"name":"Card 1","ord":0,"qfmt":"{{Front}}","afmt":"{{FrontSide}}\n\n<hr id=answer>\n\n{{Back}}","bqfmt":"","bafmt":"","did":null,"bfont":"","bsize":0}],"flds":[{"name":"Front","ord":0,"sticky":false,"rtl":false,"font":"Arial","size":20},{"name":"Back","ord":1,"sticky":false,"rtl":false,"font":"Arial","size":20}],"css":".card {\n font-family: arial;\n font-size: 20px;\n text-align: center;\n  color: black;\n  background-color: white;\n}\n","latexPre":"\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage[utf8]{inputenc}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n","latexPost":"\\end{document}","latexsvg":false,"req":[[0,"any",[0]]]},"'$dfh_model_id'":{"id":'$dfh_model_id',"name":"'$dfh_model_name'","type":0,"mod":1651314063,"usn":-1,"sortf":0,"did":null,"tmpls":[{"name":"Card 1","ord":0,"qfmt":"{{Front}}","afmt":"{{FrontSide}}\n\n<hr id=answer>\n\n{{Back}}","bqfmt":"","bafmt":"","did":null,"bfont":"","bsize":0}],"flds":[{"name":"Front","ord":0,"sticky":false,"rtl":false,"font":"Arial","size":20},{"name":"Back","ord":1,"sticky":false,"rtl":false,"font":"Arial","size":20}],"css":"'$dfh_model_css'","latexPre":"\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage[utf8]{inputenc}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n","latexPost":"\\end{document}","latexsvg":false,"req":[[0,"any",[0]]]}}')

if [ "$(jq .showDeckNameDialog $(configjson))" == "true" ] ; then
    getdeckname 
    if [[ -z $DECK_NAME_UI ]]; then
        # FIXME: return ? fix control flow...
        exit # user cancel
    else
        DECK_NAME=$(echo -n $DECK_NAME_UI | sed 's/\W/_/g')
    fi
fi

# DEFAULT_DECK_JSN=$( curl --silent -H 'Accept: application/vnd.github.v3.raw' \
#         -L https://api.github.com/repos/ankidroid/Anki-Android/contents/AnkiDroid/src/main/java/com/ichi2/libanki/Decks.java | \
#     sed -n -e '/DEFAULT_DECK = ""/,/;/p' | \
#     perl -e '$_=join("",<>);s%/\*.*?\*/%%gs;s%//.*$%%gm;print' | \
#     sed '1d' |
#     sed 's/^[[:space:]]*+[[:space:]]*//g; s/;$//g; s/^"//g; s/"[[:space:]]*$//g; s/\\"/"/g')
# FIXME construct $decks with $DEFAULT_DECK_JSN + id name mod ? "id":1,"name":"Default","mod":1649864757

# c_did=$(($(date +%s%N)/1000000))  #unique 13 "id"
c_did=$(time_ms)  #unique 13 "id"
decks=$(jq -n --compact-output '{"'$c_did'":{"id":'$c_did',"mod":1651317796,"name":"'$DECK_NAME'","usn":-1,"lrnToday":[0,0],"revToday":[0,0],"newToday":[0,0],"timeToday":[0,0],"collapsed":true,"browserCollapsed":true,"desc":"","dyn":0,"conf":1,"extendNew":0,"extendRev":0},"1":{"id":1,"mod":0,"name":"Default","usn":0,"lrnToday":[0,0],"revToday":[0,0],"newToday":[0,0],"timeToday":[0,0],"collapsed":true,"browserCollapsed":true,"desc":"","dyn":0,"conf":1,"extendNew":0,"extendRev":0}}')

dconf=$(jq -n --compact-output '{"1":{"id":1,"mod":0,"name":"Default","usn":0,"maxTaken":60,"autoplay":true,"timer":0,"replayq":true,"new":{"bury":false,"delays":[1.0,10.0],"initialFactor":2500,"ints":[1,4,0],"order":1,"perDay":20},"rev":{"bury":false,"ease4":1.3,"ivlFct":1.0,"maxIvl":36500,"perDay":200,"hardFactor":1.2},"lapse":{"delays":[10.0],"leechAction":1,"leechFails":8,"minInt":1,"mult":0.0},"dyn":false,"newMix":0,"newPerDayMinimum":0,"interdayLearningMix":0,"reviewOrder":0,"newSortOrder":0,"newGatherPriority":0}}')

tags=$(jq -n --compact-output '{}')

sqlite3 $ANKI_DB  <<EOF
    INSERT INTO col VALUES ('$id', '$crt', '$mod', '$scm', '$ver', '$dty', '$usn', '$ls',\
        '$conf', '$models', '$decks', '$dconf', '$tags');
EOF

#n=1
for entry in $UNPROCESSED
do
    ID=$(echo $entry | awk -F'|' '{print $1}')
    WORD=$(echo $entry | awk -F'|' '{print $2}')
    SOUNDFILE=$OUTDIR/sounds/$WORD.mp3
    ANKIDROID_AUDIOCLIP=$(printf "dfh_ankidroid_audioclip_%s_%d" $WORD $(($(date +%s%N)/1000000)) | sed 's/\W/_/g')
    TRANSLATION=$(echo $entry | awk -F'|' '{print $3}')

    cp  $SOUNDFILE $TEMPDIR/$ID
    
    MEDIA_JSN="$MEDIA_JSN$(printf '"'$ID'": "'$ANKIDROID_AUDIOCLIP'.mp3",')"
    
    PROCESSED="$PROCESSED word='$(echo -n $WORD | sed "s/'/''/")' or"

    n_id=$(($(date +%s%N)/1000000))  #unique 13 $(echo $(($(date +%s%N)/1000000)) )
    guid                        # new guid for every note
    n_guid=$GUID                #unique
    n_mid=$dfh_model_id
    n_mod=$(date +%s)           #unique 10
    n_usn=-1
    n_tags=""
    
    # the values of the fields in this note. separated by 0x1f (31) character
    #n_flds=$(printf "[sound:%s.mp3] %s\u001f%s" $ANKIDROID_AUDIOCLIP $(echo -n $WORD | sed "s/'/''/") $TRANSLATION)
    #n_sfld=$(printf "[sound:%s.mp3] %s" $ANKIDROID_AUDIOCLIP $(echo -n $WORD | sed "s/'/''/"))
    # FIXME hyphenation or not???
    # https://developer.android.com/reference/android/webkit/WebView#loadDataWithBaseURL(java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)
    # Anki-Android/AnkiDroid/src/main/java/com/ichi2/anki/AbstractFlashcardViewer.kt
    qword=$(echo -n $WORD | sed "s/'/''/")
    aword=$(echo -n $TRANSLATION | sed "s/'/''/")
    #FIXME move card representation n_flds n_sfld to function? add reading rules icon? click to show?
    n_flds=$(printf '<p lang="%s" style="color:#154360;">[sound:%s.mp3]%s</p>\u001f<p lang="%s">%s</p>' \
        $LANG_FROM $ANKIDROID_AUDIOCLIP $qword $LANG_TO $aword)
    n_sfld=$(printf '<p lang="%s" style="color:#154360;">[sound:%s.mp3]%s</p>' \
        $LANG_FROM $ANKIDROID_AUDIOCLIP $qword)
    
    # integer representation of first 8 digits of sha1 hash of the first field
    n_csum=$(printf "%d\n" 0x$(echo -n $n_sfld | sha1sum | awk '{print $1}' |  cut -c 1-8) )
    n_flags=0
    n_data=""
    
    c_id=$(($(date +%s%N)/1000000)) #unique 13
    c_nid=$n_id                     #unique n_id 13

    c_ord=0
    c_mod=$n_mod                    #unique n_mod 10
    c_usn=-1
    c_type=0
    c_queue=0
    c_due=1003557
    c_ivl=0
    c_factor=0
    c_reps=0
    c_lapses=0
    c_left=0
    c_odue=0
    c_odid=0
    c_flags=0
    c_data=""
CARDS_NOTES_STMT=$CARDS_NOTES_STMT$( cat << EOF
INSERT OR IGNORE INTO notes VALUES ('$n_id', '$n_guid', '$n_mid', '$n_mod', '$n_usn', '$n_tags',\
        '$n_flds', '$n_sfld', '$n_csum', '$n_flags', '$n_data');
INSERT OR IGNORE INTO cards VALUES ('$c_id', '$c_nid', '$c_did', '$c_ord', '$c_mod', '$c_usn',\
        '$c_type', '$c_queue', '$c_due', '$c_ivl', '$c_factor', '$c_reps', '$c_lapses', '$c_left', \
        '$c_odue', '$c_odid', '$c_flags', '$c_data');
EOF
)

done

# echo "ANKI_DB=$ANKI_DB"
# echo -e "\nmaking db, run: \n sqlite3 $ANKI_DB  $CARDS_NOTES_STMT"
sqlite3 $ANKI_DB  $CARDS_NOTES_STMT
echo "db created."

MEDIA_JSN="$(echo $MEDIA_JSN | sed 's/\,$//')}"
echo "$MEDIA_JSN" > $TEMPDIR/media

IFS=$IFS_BAK

getexistingdirectory
echo -e "DECK_OUTDIR:\n$DECK_OUTDIR\n"
if [[ -z $DECK_OUTDIR ]]; then
    exit    # user cancel
fi

DECK=$DECK_NAME-$DECK_TIMESTAMP.apkg

if [ $(machine) == "Linux" ] ; then
    # echo "rm -f "$DECK_OUTDIR/$DECK""
	# rm -f "$DECK_OUTDIR/$DECK"
	zip -j "$DECK_OUTDIR/$DECK"  $TEMPDIR/*
	#rm -rf $TEMPDIR
elif [ $(machine) == "Cygwin" ] ; then
	echo "cygwin... zip"
	TMP_APKG=$TMP/$TIME_MS.zip
	echo "zip -j $TMP_APKG  $TEMPDIR/*"
    zip -j $TMP_APKG  $TEMPDIR/*
	cmd /c cscript /nologo move.vbs $( cygpath -w $TMP_APKG) "$DECK_OUTDIR_FILE" "$DECK"
else
    echo -e "\nerror: zip failed...\n"
    exit
fi

# echo "rm -rf "$DECK_OUTDIR/$DECK_NAME""
# rm -rf "$DECK_OUTDIR/$DECK_NAME"
# unzip -d "$DECK_OUTDIR/$DECK_NAME" "$DECK_OUTDIR/$DECK"
echo $PROCESSED | sed 's/ or$//' 
echo "move processed history into words..."
sqlite3 "$DB_FILE" <<EOF
        INSERT OR IGNORE INTO words
        (id, word, translation)
        SELECT id, word, translation
        FROM history;
        DELETE FROM history;
        UPDATE words SET processed=1 WHERE $(echo $PROCESSED | sed 's/ or$//' );
EOF

echo "anki: Done."

if [ $(echo -n $(jq .ydupload $(configjson)) | tr -d '"')  == "true" ]; then
#     echo "upload... $DECK_OUTDIR/$DECK"
    bash $DFHHOME/yd-upload.sh "$DECK_OUTDIR/$DECK"
fi

exit
