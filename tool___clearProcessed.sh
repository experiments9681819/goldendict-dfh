#!/bin/bash

OUTDIR=$PWD/data
DB_FILE=$OUTDIR/dfh.sqlite

sqlite3 "$DB_FILE" <<EOF
    UPDATE words SET processed=0 ;
EOF
