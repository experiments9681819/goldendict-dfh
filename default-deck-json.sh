#!/bin/bash

# jq . <<EOF
# {"a":1,
# "b":true
# }
# EOF


# cat ~/Qt/Decks.java | sed -n -e '/DEFAULT_DECK =/,/;/p' 
if [ ! -f Decks.java ]; then
    echo "download..."
    curl --silent -H 'Accept: application/vnd.github.v3.raw' \
        -O \
        -L https://api.github.com/repos/ankidroid/Anki-Android/contents/AnkiDroid/src/main/java/com/ichi2/libanki/Decks.java
fi

DEFAULT_DECK_JSN=$(cat Decks.java | \
    sed -n -e '/DEFAULT_DECK = ""/,/;/p' | \
    perl -e '$_=join("",<>);s%/\*.*?\*/%%gs;s%//.*$%%gm;print' | \
    sed '1 s/.*//g' |
    sed 's/^[[:space:]]*+[[:space:]]*//g; s/;$//g; s/^"//g; s/"[[:space:]]*$//g; s/\\"/"/g')
    
# DEFAULT_DECK_JSN=$( curl -H 'Accept: application/vnd.github.v3.raw' \
#         -L https://api.github.com/repos/ankidroid/Anki-Android/contents/AnkiDroid/src/main/java/com/ichi2/libanki/Decks.java | \
#     sed -n -e '/DEFAULT_DECK = ""/,/;/p' | \
#     perl -e '$_=join("",<>);s%/\*.*?\*/%%gs;s%//.*$%%gm;print' | \
#     sed '1 s/.*//g' |
#     sed 's/^[[:space:]]*+[[:space:]]*//g; s/;$//g; s/^"//g; s/"[[:space:]]*$//g; s/\\"/"/g')

echo "$DEFAULT_DECK_JSN" | jq --compact-output '.'

