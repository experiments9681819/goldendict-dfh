Set args = Wscript.Arguments

Src=args(0)
Dst=args(1)
Fname=args(2)

call move( Src, Dst, Fname )

Sub move ( SrcStr, DstFStr, NameStr )
	Set FSO = CreateObject("Scripting.FileSystemObject")
	Const ForReading = 1
	Dim file, content
	If (fso.FileExists(DstFStr)) Then
		Set file = FSO.OpenTextFile(DstFStr, ForReading)
		DestString = Quote & Replace(file.ReadAll, vbCrLf, "") & Quote 
		Set SrcFile = FSO.GetFile(SrcStr)
		SrcFile.Move DestString & "\" & NameStr
	Else
		err = 1
		Exit Sub
	End If

End Sub
