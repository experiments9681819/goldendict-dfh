#!/bin/bash

OUTDIR=$PWD/data
DB_FILE=$OUTDIR/dfh.sqlite
STMT=""

while read line; do
    if [ "$(echo -n $line | sed '/^$/d')" == "" ]; then
        continue
    fi
    STMT="$STMT word='$(echo -n $line | sed "s/'/''/")' or"
    bash trans.sh "$line"
done < $1

sqlite3 "$DB_FILE" <<EOF
    UPDATE OR IGNORE words SET processed=0 WHERE $(echo -n $STMT | sed 's/ or$//');
EOF

bash anki.sh

echo "All Done."
exit
