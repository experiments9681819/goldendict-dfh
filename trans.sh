#!/bin/bash

# https://wiki.archlinux.org/title/GoldenDict
# https://wiki.archlinux.org/title/Dictd#Hosting_Offline_Dictionaries
# https://github.com/freedict/fd-dictionaries
# http://forum.ru-board.com/topic.cgi?forum=5&topic=49149

source $DFHHOME/dfhlib.sh

OUTDIR=$(outdir)
DB_FILE="$OUTDIR/dfh.sqlite"

SOUND_FNAME="$*"
SOUNDFILE="$OUTDIR/sounds/$SOUND_FNAME.mp3"
# echo "SOUNDFILE="$OUTDIR/sounds/$SOUND_FNAME.mp3""

ID=$(time_ms)

LANG_FROM=$(echo $(jq .langFrom $(configjson) ) | tr -d '"')
LANG_TO=$(echo $(jq .langTo $(configjson)) | tr -d '"')

if [ ! $(which trans) ];then
	echo "install trans from https://github.com/soimort/translate-shell"
	#wget git.io/trans
#     curl --location --silent --output trans git.io/trans
fi

dotranslation() {
    RESULT_WORDS=$( sqlite3 "$DB_FILE" "SELECT translation FROM words WHERE word='$(echo -n $* | sed "s/'/''/")';" )
    RESULT_HIST=$( sqlite3 "$DB_FILE" "SELECT translation FROM history WHERE word='$(echo -n $* | sed "s/'/''/")';" )
    # echo "error:_$?_"
    if [ ! -z "$RESULT_WORDS" ];then
        echo $RESULT_WORDS
    elif [ ! -z "$RESULT_HIST" ];then
        echo $RESULT_HIST
    else
        # https://github.com/soimort/translate-shell
#         TRANSLATION=$(bash "$HOME/dfh/trans" -engine google -brief $LANG_FROM:$LANG_TO "$*")
        TRANSLATION=$(trans -engine google -brief $LANG_FROM:$LANG_TO "$*")
        if [ ! "$?" -eq 0 ]; then # FIXME catch all errors!!!
            echo "error translating... no internet?"
            exit 1
        fi
        echo $TRANSLATION
        sqlite3 "$DB_FILE" <<EOF
        INSERT INTO history VALUES ('$ID', '$(echo -n $* | sed "s/'/''/")', '$TRANSLATION');
EOF
    fi
}

getsound_google() {
    local IFS=+;
    if [ ! -f "$SOUNDFILE" ]; then
    curl --silent \
        --user-agent "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/81.0" \
        --output "$SOUNDFILE" \
        "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q=$*&tl=$LANG_FROM"
    fi

    # if network error remove empty soundfile   
    test -s "$SOUNDFILE"
    if [ $? -eq 1 ]; then
        rm -f "$SOUNDFILE"
    fi
}

getsound_yandex() {
    local IFS=+;
# --crlfile <file>
#  --cacert <file>
#  --dump-header <filename>
# curl --libcurl client.c https://example.com
#  -L, --location
# -G, --get
#  -s, --silent

# YANDEX
# ykey="YOUR_API_KEY"
# ytext=$(curl -s -G "https://translate.yandex.net/api/v1.5/tr.json/translate" \
#     -d "key=$ykey&lang=$from-$to"  --data-urlencode "text=$text" | grep -Po \
#     '"text":\[".*?"\]'|sed 's/"text"\:\["//g'|sed 's/"\]$//'|sed 's/\\"/"/g')
# echo "$ytext";
}

if [ "$#" -lt 1 ];then
        echo "Usage: $0 word to translate"
        exit 1
elif [ ! -f "$DB_FILE" ]; then 
    #echo "no history database $DB_FILE <br> creating new...<br><br>"
    mkdir -p "$OUTDIR/sounds"
    sqlite3 "$DB_FILE"  "
        CREATE TABLE IF NOT EXISTS  history (
                id integer PRIMARY KEY,
                word TEXT UNIQUE,
                translation TEXT NOT NULL);
        CREATE TABLE IF NOT EXISTS words(
                id integer PRIMARY KEY,
                word TEXT UNIQUE,
                translation TEXT NOT NULL,
                processed BOOLEAN CHECK (processed IN (0, 1))
                );"
    dotranslation $*
    getsound_google $*
else
    dotranslation $*
    getsound_google $*
fi

exit 0
