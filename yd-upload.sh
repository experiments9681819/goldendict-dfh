#!/usr/bin/env bash
# DFHHOME=~/Projects/goldendict-dfh-experiment/src/dfh
# shellcheck source=/home/vic/Projects/goldendict-dfh-experiment/src/dfh/dfhlib.sh
source "$DFHHOME"/dfhlib.sh

function ydprogress(){

ydcmd ls | grep dir | grep "$YD_DIR"  > /dev/null 2>&1
ERROR=$?
if [ "$ERROR" -ne "0" ]; then
       ydcmd mkdir disk:/"$YD_DIR"
fi

ERROR=$?
if [ "$ERROR" -ne "0" ]; then
       echo -e "ydcmd: fail to create remote directory $ERROR\n"
       exit
fi

#ydcmd --dbus="$PB_ID" put $FILE $UPLOAD_PATH
echo "ydcmd  put $FILE $UPLOAD_PATH"
ydcmd  put "$FILE" "$UPLOAD_PATH"


# qdbus $PB_ID org.freedesktop.DBus.Properties.Set '' 'value' 5
# sleep 2
# qdbus $PB_ID org.freedesktop.DBus.Properties.Set '' 'value' 7
# sleep 2
# qdbus $PB_ID org.freedesktop.DBus.Properties.Set '' 'value' 10
# qdbus $PB_ID close

# TODO: https://develop.kde.org/docs/administration/kdialog/#progress-dialogs
# dbusRef=`kdialog --progressbar "Press Cancel at Any time" 10`
# qdbus $dbusRef showCancelButton true
#
# until test "true" = `qdbus $dbusRef wasCancelled`; do

# FIXME: test it
 # ydcmd  put "$FILE" "$UPLOAD_PATH"

#  sleep 1
#  inc=$((`qdbus $dbusRef Get "" "value"` + 1))
#  qdbus $dbusRef Set "" "value" $inc;
# done
#
# qdbus $dbusRef close
}

FILE=$1
if [ -z $FILE ]; then
  echo "Nothing to upload..."
else
#   echo "uploading: --$FILE--"
  YD_DIR=$(echo -n $(jq .ydDir $(configjson)) | tr -d '"')
  if [ ! -z $YD_DIR ]; then
    UPLOAD_PATH="disk:/$YD_DIR/"
  else
    UPLOAD_PATH=
  fi
#   echo "to --$UPLOAD_PATH--"
#   PB_ID=$(kdialog  --progressbar "Uploading..." 10 --title "ydcmd" )
#   echo $PB_ID
  ydprogress
fi
echo "Upload done."
