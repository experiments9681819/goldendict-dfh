[dfh] is a wrapper for goldendict, it makes deck from history

this is feature proposal for goldendict.org dictionary

Setup:

## Linux:
```
apt-get install goldendict jq sqlite3 zip sed awk bash 
```

download [dfh] & extract to ~/

```
cd ~/dfh
bash goldendict-dfh.sh $(which goldendict)
```

in goldendict:  edit > dictionaries > programs

audio
tts.sh %GDWORD%

html
trans.sh %GDWORD%


## Legacy:

https://www.msys2.org/
1. Unzip the package and create a new folder armgcc under the /msys64/usr folder. Then copy all the files in the decompression package gcc-arm-none-eabi to the armgcc folder
2. profile file under the MSYS64/etc/ folder, open it with a text editor, and finally enter export PATH="/usr/armgcc/bin:$PATH", and save and exit after completion


https://sourceforge.net/projects/mingw-w64/files/External%20binary%20packages%20%28Win64%20hosted%29/MSYS%20%2832-bit%29/MSYS-20111123.zip/download
mingw-get install msys-zip
mingw-get show | grep "Package: "


https://stackoverflow.com/questions/38393755/mingw-w64-offline-installer



https://askubuntu.com/questions/47775/how-can-i-set-the-path-variable-in-a-desktop-file-to-be-relative-to-the-locat






download [setup-x86-2.874.exe] & move to %HOMEDRIVE%HOMEPATH%

start > run > cmd > enter

```
cd %HOMEDRIVE%HOMEPATH%
setup-x86-2.874.exe --no-verify --no-shortcuts --quiet-mode --only-site  \
    --site http://ctm.crouchingtigerhiddenfruitbat.org/pub/cygwin/circa/2016/08/30/104223  \
    --arch x86 --root C:\cygwin --packages bash,jq,sqlite3,zip,curl,sed,awk,grep,test
```

download [mplayer] Build (r38363+g4fbf3c828b) Generic

extract MPlayer-generic-r38363+g4fbf3c828b.7z to C:\cygwin\opt

WIN+R

```
C:\cygwin\Cygwin.bat > enter
ln -s /opt/MPlayer-generic-r38363+g4fbf3c828b/mplayer.exe /usr/bin/
```

download [goldendict] GoldenDict-1.5.0-RC2-372-gc3ff15f_(QT_486).7z & extract to %HOMEDRIVE%HOMEPATH%

download [dfh] & extract to %HOMEDRIVE%HOMEPATH%


WIN+R

```
C:\cygwin\Cygwin.bat > enter
cd "$HOMEDRIVE$HOMEPATH\dfh"
bash goldendict-dfh.sh ../GoldenDict/GoldenDict.exe
```
in goldendict  edit > source > programs

audio
tts.bat %GDWORD%

html
trans.bat %GDWORD%


Usage:

1. translate words with goldendict`
2. exit with Ctrl+Q

on exit dfh script should make anki deck from words left in goldendict history

create desktop shortcut:
```
bash makeDesktopShortcut.sh 
```

[dfh]: https://sr.ht/~vd31681/dfh
[ankidroid]: https://github.com/ankidroid/Anki-Android/wiki/Database-Structure
[translate-shell]: https://github.com/soimort/translate-shell
[setup-x86-2.874.exe]: http://ctm.crouchingtigerhiddenfruitbat.org/pub/cygwin/setup/snapshots/setup-x86-2.874.exe
[mplayer]: https://oss.netfarm.it/mplayer/
[goldendict]: https://github.com/goldendict/goldendict/wiki/Early-Access-Builds-for-Linux-Portable

